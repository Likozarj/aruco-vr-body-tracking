# THIS IS AN OLD VERSION. DO NOT USE IT. USE THE IMPROVED VERSION:

https://github.com/ju1ce/April-Tag-VR-FullBody-Tracker/

# VR full body tracking using Aruco markers

This is my attempt at creating a full-body tracking solution using resources availible to everyone - a phone and some cardboard. The program uses a free video streaming app on the phone (IP Webcam on android) to stream video to our computer, where position of markers is detected with OpenCV and then sent to SteamVR. Using a newer high-end phone, it works with high precision and speed even with smaller markers. On older phones, bigger markers may need to be used and speed may be slower, but it should still work. This program is still experimental and may have bugs, so please message me if you can't get something working.

Example video: [TODO]

# Installation

While the installation should be fairly quick and straight-forward, there are many steps to it and many things that can go wrong. I suggest every step should be tested first to ensure its working properly before proceeding.

## Downloading a video streaming app on the phone

First step is making a connection between our phone and computer. For this to work, both the PC and the phone must be connected to the same network: this usualy means the computer's ethernet cable will be plugged into the same router that your phone's wifi is connected to, or that both are connected to the same wifi network.

On android, the best app for our purposes is IP Webcam, which you can download from the play store. Once you open it, navigate to Video preferences to set up the camera. The most important field is the video resolution: We should set it to something around 1440x1080 or 1600x1200, but ensure that it is in 4:3 aspect ratio. If your playspace is smaller, you may want to set the Video orientation to portrait. Other settings are unimportant and should be left at default.

Afterwards, go back to main screen, scroll down and click on start server. This should start the video stream, and on the bottom, your phone's IP address should be shown. It should be something like:

```
http://192.168.1.100:8080
```

To test the connection, you can imput this into your browser like a website. This should open a page with your stream (click the Browser video renderer) and you should see the phone's camera. Ensure that the stream is smooth without lag before proceeding.

I have not tested this on IOS phones, but there should be a similar app you can use there with similar steps

I am still testing diffrent approaches to use a wired connection, but I have not found a reliable solution yet. If you know of a good app to turn a phone into a wired webcam, please follow the instructions under Webcam on the bottom. Also, please message me about it so I can test it myself.

## Downloading and starting my program

Once the video stream works reliably, you can download my program on https://www.dropbox.com/sh/7nc4pr077x0pic2/AACkgt9hBzz1MYJoDCsYtXkOa?dl=0. Extract the entire contents into a folder. To start the program, simply launch Aruco_Tracker.exe. When you start it for the first time, it will inform you that the parameter file was not found, and ask you to imput the stream IP adress. Ensure that the stream is running, then input the same IP as above, but add /video on the end:

```
http://192.168.1.100:8080/video
```

Then, it will ask for the size of your markers. Note that the size should be in meters! If you havent printed them yet, just input something like 0.05 and we will change it later. Pressing enter will connect to your phone and start the program. Ignore the settings window that will show up, we will be using it later.

If an error occurs, the most common problems are:
    - The stream on the phone isn't running.
    - The stream IP is written wrong. Check all the numbers and that http:// is at the beginning and /video is added at the end
    - The opencv_videoio_ffmpeg430_64.dll file is not in the same directory as the Aruco_Tracker.exe.
    
If the stream is working correctly in browser but not in my program, please message me.

![terminal](/images/Image_connect.PNG)

How the terminal should look like after a successfull connection.

## Calibrating the camera

Most of the user interface is inputing a number and pressing enter. Before we do anything else, we must calibrate our camera. For this, input 6 and press enter. Two image windows should open: one containing the image stream, the other a calibration grid. To calibrate our camera, we should take 20 pictures of the calibration grid from as many diffrent angles as possible, and from diffrent distances. To take a picture, ensure one of the windows is in focus and press any button on the keyboard. 

![calibration step 1](/images/Image_calib_step1.PNG)

Move the windows as shown (if you have two screens, I suggest you put the out window into the other screen). Click on the calibration grid to ensure it is in focus, aim at the grid with your phone, and press any button.

![calibration step 2](/images/Image_calib_step2.PNG)

Click on the out window to check the detected corners. There is no problem if not all were recognised, but if a lot of them are outside the grid, you may have to restart the calibration. Press any button to go back to picture mode.

Now move the phone a little bit, still aiming at the grid, then repeat the process 20 times. The number of successful pictures taken will be outputed on the terminal, and once there are 20, the camera will be calibrated and the parameter file will be created. I suggest to close the terminal and restart the program to ensure it was done correctly - if it was, the program will no longer ask for the IP adress, but rather just print the calibration matrix.

## Saving a marker

The program enables you to save multiple markers to act as a single tracker to improve accuracy and to enable detection from more angles. But first, we should check if it works. Open one of the marker images in the folder and press 4 in our program to start the marker saving. A window showing our camera stream should appear. 

![markers step 1](/images/Image_markers_step1.PNG)

Aim the phone at the markers on the screen to detect them. Press any button while the window is in fokus to take a picture.

![markers step 2](/images/Image_markers_step2.PNG)

Once you take the first picture, one of the markers will be added as the main one - it will have longer axis than the others.Now, every time you take a picture, all of the detected markers will be added to the group.

![markers step 3](/images/Image_markers_step3.PNG)

This group can now be detected even if some of the markers are not visible. 

![markers step 4](/images/Image_markers_step4.PNG)

If some markers were saved incorrectly (or, in this case, are detected twice) the main axis will be placed incorrectly. If this happens despite you only having the correct markers on screen, something went wrong. You should exit the program and try again.

Once all markers are added, press ESC to save them.

## Connecting to SteamVR

The program should now be setup, now lets try to connect it to SteamVR. Copy the trackers map from the downloaded folder and move it into steam install folder/steamapps/common/steamvr/drivers. Now, with your headset connected and active, start Aruco_Test.exe and press 2. The text "waiting" should appear. Now, start SteamVR. 

![connect step 1](/images/Image_connect_step1.PNG)

The console should now write connected pipe 1. When steamVR starts, it will show a marker connected. Press 1 on the terminal to start the program, put the settings window somewhere on the screen, then put on your headset.In steamVR virtual desktop, move the calibration slider to 1.

![connect step 2](/images/Image_connect_step2.PNG)

If everything is working correctly, an arrow should appear in the center of your playspace (or sometimes somewhere else, but it should be somewhere). If it does, everything works correctly and you can now print your markers!

If you are using an oculus headset, do not start the calibration (putting the calibration slider to 1) before you use your actual oculus controllers! SteamVR will use the first tracker that moves as your left controller, so ensure you can use both controllers before you start.

If the program shows pipe connected, but the tracker is not shown on steamVR, you may need to enable multiple drivers. Go to your steam install directory (just steam, not SteamVR!), then to the config folder. It should be something like: "C:\Program files\config". Open "steamvr.vrsettings" with notepad. Go to the "steamvr" part and add ""activateMultipleDrivers" : true,", so it will look something like this:
```
"steamvr" : {
      "activateMultipleDrivers" : true,
      "allowSupersampleFiltering" : false,
      "forceFadeOnBadTracking" : false,
      "haveStartedTutorialForNativeChaperoneDriver" : true,
      "installID" : "10664029049040693504",
      "lastVersionNotice" : "1.12.5",
      "lastVersionNoticeDate" : "1590713293",
      "overlayRenderQuality_2" : 1,
      "showAdvancedSettings" : true,
      "showMirrorView" : false,
      "showPerfGraph" : true,
      "supersampleManualOverride" : true,
      "supersampleScale" : 4.3600000000000003
   },
```

## Testing marker detection

Before glueing the markers to cardboard, I suggest you simply print the markers_high_1 paper, fold it in half (so 6 markers are seen at a time) and try how well detection works with that. When printing markers ensure that the border is the same size as the black border on the markers! First remove all markers that you may have added before (open the params.yml file and remove the entire "markers:" part). Then add one side with 6 makers with the process described above. Follow the above steps to connect to SteamVR again, then follow the steps bellow (Calibrating playpace, just imagine im holding a piece of paper with markers rather than the cardboard marker).

Now check how well the detection works. Some shaking is expected (5-10 cm per frame). If you can move the paper around without losing detection, great, you can go to the high performance markers step! If not, there is still some things you can try:

First, ensure your room is as brightly lit as it can be and that most of the light is coming from the same direction that your phone/camera is - otherwise, there can be some glare on the markers that will prevent detection. Ensure there is no light from behind you as well.

Detection can also sometimes fail if your background is too light - if markers are not detected when in front of a white surface, try to use the camera/phone from another direction.

If that did not help detection or if the markers are still no longer detected after even slight movements, you may have to use low performance markers.

## High performance markers

If your camera can use the better markers, print the markers_high_2 paper as well (again, ensure there is a thick enough white border around them!). Cut both markers in half, then use half of the paper for each marker. glue the marker along the instructions in the "Creating the markers" section.

To increase performance, you will also probably want to change some parameters in the params.yml file. Open it with notepad, and change: cornerRefineMethod to 2, usePredictive to 1, redetectMarkers to 1. To reduce shaking at the cost of speed, increase numOfPrevValues to 10 - you can later fine-tune this value to get the best shaking-speed ratio. You can take a look at what all these params are on the bottom.

## Low performance markers

If the markers were too small to be detected reliably, print the 3 markers_low_ papers and use those in the "Creating the markers" instructions. Again, ensure that the border of the paper is about as thick as the black border of the markers or detection will be bad!

To increase performance, you will also probably want to change some parameters in the params.yml file. Open it with notepad, and change: cornerRefineMethod to 2. Others that may increase performance are also: usePredictive to 1, useOpticalFlow to 1, but you may have to try these out before seeing if they actualy work well. To reduce shaking at the cost of speed, increase numOfPrevValues to 10 - you can later fine-tune this value to get the best shaking-speed ratio. You can take a look at what all these params are on the bottom.

## Creating the markers

Markers are best made out of hard cardboard with the marker images glued to it. If you have a good camera on your phone, you can print the 2 images in the folder directly and cut them in half - each piece should be able to be one marker. If markers of this size are not detected well enough, you can try printing them bigger.

![markers 1](/images/Image_markers_1.PNG)
![markers 2](/images/Image_markers_2.PNG)

Glue the markers together as shown. Make sure they are as sturdy as possible, they shouldn't bend or the detection will be worse.

![markers_3](/images/Image_markers_big.PNG)

In case you have a worse camera with a lot of motion blur, you may have to make markers of this size. The ones on the image are older versions, however - ensure the white border is thicker and that the brown cardboard is not seen, as it will decrease performance.

Once markers are made, we have to save each one in the program. Use the procedure in the saving markers step. When taking a picture to add a marker, make sure at least one marker that is already added to the group is also visible.

## Calibrating playspace

To use our markers as trackers, we need to calibrate our camera space to steamVR space. To do this, first connect to steamVR as in the above step and set calibration to 1 in the settings. The arrow should appear in the middle of your playspace. The arrow represents your marker - to calibrate the system, try to match your marker with it like in the photos. When it is in the correct position, set calibration back to 0. If the arrow is outside your playspace, you can use the bottom 3 sliders in the settings to move the arrow to a place you can reach. 

After calibrating, you can try moving the marker around and it should be synchronised with the arrow. Since the completely correct rotation is hard to guess the first time, your marker may get desynchronized if it moves out of the origin - if the difference between where the arrow is and where the irl marker is is too big, you can always try to recalibrate by putting calibration to 1 again. (sometimes, however, the arrow will not be rotated correctly)

The first marker that you add will always be the one used for calibration.

![playspace_1](/images/Image_space_calib_out.png)

This is how the calibration looks like from your perspective outside vr

![playspace_2](/images/Image_space_calib_hmd.png)

This is how the calibration looks like from your perspective inside vr

![playspace_3](/images/Image_space_calib_phone.png)

This is how the calibration looks like on the video stream.

That should be it! You can now be an anime girl in VRchat! If things are not working as expected or you want to change your ip/marker size, check the extras bellow:

### Other stuff

## Improving performance

If the detection works, but is performing poorly, there is some things you can do to try to improve it. First, in the settings (check the changing the settings bellow), change detectorParams/cornerRefinementMethod to 2. (if you want to check what that means, you can take a look at oficial aruco tutorial.(1 = subpix, 2=contour,3=aprilTag, but that is not important if you dont know what that means)). This should improve the marker detection.

Another thing you can try, especialy on newer phones, is to change the exposure setting of your camera. If you connect to your phone camera in your browser, there should be a field to change exposure and ISO. Putting the ISO as high as you can without ruining the image and putting the exposure as low as you can without making the image too dark can completely eradicate motion blur, meaning you can move the marker as fast as you can and it will be detected in every frame. (on my Pocophone, i could use exposure of as low as 3ms). But beware that too grainy images (beacause of ISO) can cause detection to fail and too dark images (exposure) can make the detection take longer. This step is usualy not necesary, but it may help in some cases.

## Using a webcam

If you have a quality webcam (or managed to use a phone as a wired webcam) you can also use that instead of an video streaming app on your phone. To do that, instead of inputting your phone IP adress, input a single-digit number representing the index of the webcam (the first connected webcam is 0, second is 1 etc). This will usualy be 0, but if that doesnt work, try the next few numbers as well. Some apps for phone connection, however, don't seem to work with this.

## Changing the settings

In the directory of the program, a params.yml file has been created - this includes all the parameters of the program, including the IP adress and the size of markers. You can open it with notepad. While in most cases, the other settings work best at default, diffrent cameras and lightning conditions may require you to change some stuff for optimal performance. This is a quick description of main settings:

- IP: the address of your phone, or the index of your webcam. Change this if the ip of your phone has changed.
- markerSize: The size of your markes. Note that the size is used when the marker groups are being saved, not when the program is running (when you are saving markers with 4, not when you run program with 1). This means that diffrent marker groups can use markers of diffrent sizes, but all markers within a group must be the same.
- usePredictive: whether the program should use previous position data when calculating position of marker or not. Using this on 1 means the marker will usualy be more stable and the chance of the position being calculated wrong is smaller, but when it does, the marker will stay wrong untill the camera can no longer see it. Should usualy be 1, especialy if you use many smaller markers, but may be better on 0 if you use fewer bigger markers.
- cameraMatrix and distCoeffs: the parameters of your camera. You shouldn't change this directly, but only through camera calibration.
- driverSmoothFactor: how much smoothing should be happening on the driver side (since our program is only 30 fps, the driver interpolates data to your headsets FPS). Default is good on Rift S, but may need adjusting on faster headsets
- smoothFactor: if smoothingType is set to 0, this setting is used. The higher this value is, the more responsive marker movement will be, but there will be more shaking. Should be 0-1
- numOfPrevValues: if smoothingType is set to 1, this setting is used. The higher this setting, the smoother will be the movement with less shaking, but the responsiveness will be slower. Should be 1-20. Change this setting to the lowest it can go while the shaking is still not noticable.
- smoothingType: the type of smoothin to use. If on 0, the position of our marker is calculated by: smoothFactor * current marker position + (1 - smoothFactor) * previous marker position. If on 1, the position is calculated by fitting a line to last numOfPrevValues positions and the current position, then using the line to calculate. There shouldn't be a reason to have this on 0.
- markers: the data of which markers are in a marker group and their relative positions. If you want to remove a marker, simple delete it from here, but do not add anything directly. If you want to delete all the marker groups, make sure to delete the "markers:" line as well.
- detectorParams: the parameters of the aruco marker detector. You should check the official "detecting aruco markers in OpenCV" tutorial if you want to know exactly what each one does.
- redetectMarkers: whether we should try to detect other markers in a group based on the positions of the detected ones. Should usualy stay 1.
- useOpticalFlow: whether we should try to track markers from previous frame without actualy reading it. Is usualy way too slow to use, but may work if you use fewer bigger markers and your camera has a lot of motion blur. Otherwise, there is no point.