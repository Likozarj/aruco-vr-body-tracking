#include "FakeTracker.hpp"
#include <cmath>

FakeTracker::FakeTracker() : 
	_pose( {0} )
{
	// Create some random but unique serial
	_serial = "ft_" + std::to_string(std::chrono::system_clock::now().time_since_epoch().count());

	//set all our varaibles to default values

	a = 0;
	b = 0;
	c = 0;
	x = 0;
	y = 0;
	z = 0;
	w = 0;

	//ensure tracker is not updating before we send our first data to it
	isSetup = false;

	// Set up some defalt rotation pointing down -z
	_pose.qRotation.w = 1.0;
	_pose.qRotation.x = 0.0;
	_pose.qRotation.y = 0.0;
	_pose.qRotation.z = 0.0;

	_pose.qWorldFromDriverRotation.w = 1.0;
	_pose.qWorldFromDriverRotation.x = 0.0;
	_pose.qWorldFromDriverRotation.y = 0.0;
	_pose.qWorldFromDriverRotation.z = 0.0;

	_pose.qDriverFromHeadRotation.w = 1.0;
	_pose.qDriverFromHeadRotation.x = 0.0;
	_pose.qDriverFromHeadRotation.y = 0.0;
	_pose.qDriverFromHeadRotation.z = 0.0;

	// To ensure no complaints about tracking
	_pose.poseIsValid = true;
	_pose.result = vr::ETrackingResult::TrackingResult_Running_OK;
	_pose.deviceIsConnected = true;
}

std::shared_ptr<FakeTracker> FakeTracker::make_new()
{
	return std::shared_ptr<FakeTracker>(new FakeTracker());
}

std::string FakeTracker::get_serial() const
{
	return _serial;
}

void FakeTracker::newValues(int id, double aa, double bb, double cc, double xx, double yy, double zz, double ww)
{
	// Update time delta (for working out velocity)
	std::chrono::milliseconds time_since_epoch = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	double time_since_epoch_seconds = time_since_epoch.count() / 1000.0;
	double pose_time_delta_seconds = (time_since_epoch - _pose_timestamp).count() / 1000.0;

	_pose_timestamp = time_since_epoch;

	// Copy the previous position data
	//double previous_position[3] = { 0 };
	//std::copy(std::begin(_pose.vecPosition), std::end(_pose.vecPosition), std::begin(previous_position));

	//calculate velocity from last position
	_pose.vecVelocity[0] = (aa - a) / pose_time_delta_seconds;
	_pose.vecVelocity[1] = (bb - b) / pose_time_delta_seconds;
	_pose.vecVelocity[2] = (cc - c) / pose_time_delta_seconds;

	//update our position/rotation values to new ones
	a = aa;
	b = bb;
	c = cc;
	x = xx;
	y = yy;
	z = zz;
	w = ww;

	//set setup to true so tracker begins updating
	isSetup = true;

	//update(0);
}

void FakeTracker::update(double factor)
{
	//dont update if we havent received values yet
	if (!isSetup)
		return;

	//dot product for lerping quaternions correctly
	double dot = w * _pose.qRotation.w + x * _pose.qRotation.x + y * _pose.qRotation.y + z * _pose.qRotation.z;

	//move our position and rotation from current to wanted position by our factor
	if (dot < 0)
	{
		_pose.qRotation.w = factor * w - (1 - factor) * _pose.qRotation.w;
		_pose.qRotation.x = factor * x - (1 - factor) * _pose.qRotation.x;
		_pose.qRotation.y = factor * y - (1 - factor) * _pose.qRotation.y;
		_pose.qRotation.z = factor * z - (1 - factor) * _pose.qRotation.z;
	}
	else
	{
		_pose.qRotation.w = factor * w + (1 - factor) * _pose.qRotation.w;
		_pose.qRotation.x = factor * x + (1 - factor) * _pose.qRotation.x;
		_pose.qRotation.y = factor * y + (1 - factor) * _pose.qRotation.y;
		_pose.qRotation.z = factor * z + (1 - factor) * _pose.qRotation.z;
	}

	_pose.vecPosition[0] = factor * a + (1 - factor) * _pose.vecPosition[0];
	_pose.vecPosition[1] = factor * b + (1 - factor) * _pose.vecPosition[1];
	_pose.vecPosition[2] = factor * c + (1 - factor) * _pose.vecPosition[2];

	//normalize our rotation
	double rotNorm = _pose.qRotation.w * _pose.qRotation.w
		+ _pose.qRotation.x * _pose.qRotation.x
		+ _pose.qRotation.y * _pose.qRotation.y
		+ _pose.qRotation.z * _pose.qRotation.z;

	double rotMag = sqrt(rotNorm);

	_pose.qRotation.w = _pose.qRotation.w / rotMag;
	_pose.qRotation.x = _pose.qRotation.x / rotMag;
	_pose.qRotation.y = _pose.qRotation.y / rotMag;
	_pose.qRotation.z = _pose.qRotation.z / rotMag;

	// If we are still tracking, update openvr with our new pose data
	if (_index != vr::k_unTrackedDeviceIndexInvalid)
	{
		vr::VRServerDriverHost()->TrackedDevicePoseUpdated(_index, _pose, sizeof(vr::DriverPose_t));
	}
}

vr::TrackedDeviceIndex_t FakeTracker::get_index() const
{
	return _index;
}

void FakeTracker::process_event(const vr::VREvent_t& event)
{
}

vr::EVRInitError FakeTracker::Activate(vr::TrackedDeviceIndex_t index)
{
	// Save the device index
	_index = index;
	
	// Get the properties handle for our controller
	_props = vr::VRProperties()->TrackedDeviceToPropertyContainer(_index);

	// Set our universe ID
	vr::VRProperties()->SetUint64Property(_props, vr::Prop_CurrentUniverseId_Uint64, 2);

	// Add our controller components. (These are the same as the regular vive controller)
	vr::VRDriverInput()->CreateBooleanComponent(_props, "/input/system/click", &_components._system_click);
	vr::VRDriverInput()->CreateHapticComponent(_props, "/output/haptic", &_components._haptic);

	// Set our controller to use the vive controller render model
	vr::VRProperties()->SetStringProperty(_props, vr::Prop_RenderModelName_String, "arrow");

	return vr::VRInitError_None;
}

void FakeTracker::Deactivate()
{
	// Clear device id
	_index = vr::k_unTrackedDeviceIndexInvalid;
}

void FakeTracker::EnterStandby()
{
}

void * FakeTracker::GetComponent(const char * component)
{
	// No extra components on this device so always return nullptr
	return nullptr;
}

void FakeTracker::DebugRequest(const char * request, char* response_buffer, uint32_t response_buffer_size)
{
	// No custom debug requests defined
	if (response_buffer_size >= 1)
		response_buffer[0] = 0;
}

vr::DriverPose_t FakeTracker::GetPose()
{
	return _pose;
}

void FakeTracker::set_pose(vr::DriverPose_t new_pose)
{
	_pose = new_pose;
}
