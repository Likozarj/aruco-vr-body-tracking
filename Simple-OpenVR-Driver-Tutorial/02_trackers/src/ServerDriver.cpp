#include "ServerDriver.hpp"
#include <windows.h>
#include <thread>
#include <sstream>
#include <iostream>
#include <string>

ServerDriver* ServerDriver::_instance = nullptr;

std::vector<HANDLE> hpipe;
int pipeNum = 1;
double smoothFactor = 0.2;
char buffer[1024];
DWORD dwWritten;
DWORD dwRead;


ServerDriver::ServerDriver()
{
}

ServerDriver* ServerDriver::get()
{
	if (_instance == nullptr)
		_instance = new ServerDriver();
	return _instance;
}

vr::EVRInitError ServerDriver::Init(vr::IVRDriverContext * driver_context)
{
	//on init, we try to connect to our pipes
	for (int i = 0; i < pipeNum; i++)
	{
		//MessageBoxA(NULL, "It works!  " + pipeNum, "Example Driver", MB_OK);
		HANDLE pipe;
		//pipe name, same as in our server program
		std::string pipeName = "\\\\.\\pipe\\TrackPipe" + std::to_string(i);

		//open the pipe
		pipe = CreateFileA(pipeName.c_str(),
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);

		if (pipe == INVALID_HANDLE_VALUE)
		{
			//if connection was unsuccessful, return an error. This means SteamVR will start without this driver running
			return vr::EVRInitError::VRInitError_Driver_Failed;
		}

		//wait for a second to ensure data was sent and next pipe is set up if there is more than one tracker
		Sleep(1000);

		//read the number of pipes and smoothing factor from the pipe
		if (ReadFile(pipe, buffer, sizeof(buffer) - 1, &dwRead, NULL) != FALSE)
		{
			//we receive raw data, so we first add terminating zero and save to a string.
			buffer[dwRead] = '\0'; //add terminating zero
			std::string s = buffer;
			//from a string, we convert to a string stream for easier reading of each sent value
			std::istringstream iss(s);
			//read each value into our variables
			iss >> pipeNum;
			iss >> smoothFactor;
		}
		//save our pipe to global
		hpipe.push_back(pipe);
	}
	
	if (vr::EVRInitError init_error = vr::InitServerDriverContext(driver_context); init_error != vr::EVRInitError::VRInitError_None) {
		return init_error;
	}
	
	for (int i = 0; i < pipeNum; i++) {
		//create a tracker object for each pipe we have.
		_trackers.push_back(FakeTracker::make_new());
		vr::VRServerDriverHost()->TrackedDeviceAdded(_trackers.back()->get_serial().c_str(), vr::TrackedDeviceClass_GenericTracker, _trackers.back().get());
	}
	
	return vr::EVRInitError::VRInitError_None;
	
}

void ServerDriver::Cleanup()
{
}

const char * const * ServerDriver::GetInterfaceVersions()
{
	return vr::k_InterfaceVersions;;
}

void ServerDriver::RunFrame()
{
	//this code is run every frame of our headset
	int i = 0;
	for (auto& tracker : _trackers) {
		//for each tracker, we look at whether there is any data on our pipe
		if (PeekNamedPipe(hpipe[i], NULL, 0, NULL, &dwRead, NULL) != FALSE)
		{
			//if data is ready,
			if (dwRead > 0)
			{
				//we go and read it into our buffer
				if (ReadFile(hpipe[i], buffer, sizeof(buffer) - 1, &dwRead, NULL) != FALSE)
				{

					buffer[dwRead] = '\0'; //add terminating zero
					//convert our buffer to string
					std::string s = buffer;

					//first three variables are a position vector
					double a;
					double b;
					double c;

					//second four are rotation quaternion
					double qw;
					double qx;
					double qy;
					double qz;

					//convert to string stream
					std::istringstream iss(s);

					//read to our variables
					iss >> a;
					iss >> b;
					iss >> c;
					iss >> qw;
					iss >> qx;
					iss >> qy;
					iss >> qz;
					//send the new position and rotation from the pipe to the tracker object
					tracker->newValues(i, a, c, b, qx, qy, qz, qw);

				}
			}
		}
		//every frame, we update the tracker whether there was new data or not
		tracker->update(smoothFactor);
		i++;
	}
		
	//original code, not sure what it does
	vr::VREvent_t event;
	while (vr::VRServerDriverHost()->PollNextEvent(&event, sizeof(event))) {
		for (auto& tracker : _trackers) {
			if (tracker->get_index() == event.trackedDeviceIndex)
				tracker->process_event(event);
		}
	}
	

}

bool ServerDriver::ShouldBlockStandbyMode()
{
	return false;
}

void ServerDriver::EnterStandby()
{
}

void ServerDriver::LeaveStandby()
{
}

