// Title: Create a coloured image in C++ using OpenCV.

#define _CRT_SECURE_NO_DEPRECATE

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <windows.h>
#include <sstream>
#include "quaternion.cpp"
#include <chrono>
#include <opencv2/video/tracking.hpp>
#include <opencv2/aruco/charuco.hpp>

#include <ctype.h>
#include <iostream>
#include <thread>

#include <fstream>

using namespace cv;
using namespace std;

struct quaternion
{
	double x;
	double y;
	double z;
	double w;
};

struct Marker
{
	int id;
	int group;
	Vec3d posOffest;
	Vec3d rotOffset;
};

int saveParams();
int detect();
int connect();
int calibrate(Mat , Mat , Ptr<aruco::Dictionary> , Mat , Ptr<aruco::DetectorParameters> );
int calibrateCamera();
int calibrateCameraCharuco();
int addMarker();
int tresholdTest();
int linearFitTest();
void llsq(int, double[], double[], double&, double&);
void getImages(VideoCapture);

Mat eulerAnglesToRotationMatrix(double, double, double);
Quaternion<double> mRot2Quat(const Mat&);
Quaternion<double> rodr2quat(double, double, double);
Quaternion<double> slerp(Quaternion<double>, Quaternion<double>, double);

Mat wtranslation;
Quaternion<double> wrotation;

int recalibrate = 0;

bool connectedPipe = false;
bool calibrated = false;

vector<HANDLE> hpipe;
int pipeNum = 1;
char buffer[1024];
DWORD dwRead;
DWORD dwWritten;

int axisSize = 1;

Mat retImage;
bool imgReady = false;

Mat cameraMatrix, distCoeffs;

vector<Ptr<aruco::Board>> markerGroups;

//double markerSize = 0.075;
//double markerSize = 0.059;
double markerSize = 0.067;
bool usePredictive = false;

double smoothFactor = 0.2;
int numOfPrevValues = 5;
double driverSmoothFactor = 0.2;
int smoothingType = 1;
bool redetectMarkers = false;
bool useOpticalFlow = false;

int calibrate_x = 50;
int calibrate_y = 60;
int calibrate_z = 50;

cv::Ptr<cv::aruco::Dictionary> dictionary;
Ptr<aruco::DetectorParameters> params;

TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
Size winSize(31, 31);

string videoStreamAddress;

inline double SIGN(double x) {
	return (x >= 0.0f) ? +1.0f : -1.0f;
}

inline double NORM(double a, double b, double c, double d) {
	return sqrt(a * a + b * b + c * c + d * d);
}

int main(int argc, char** argv)
{
	dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);	//load default 4x4 marker dictionary

	params = aruco::DetectorParameters::create();		//creating default detector parameters
	params->markerBorderBits = 2;						//look at opencv aruco docs for what each one is
	params->adaptiveThreshWinSizeMin = 23;
	params->adaptiveThreshWinSizeMax = 33;
	params->adaptiveThreshWinSizeStep = 10;
	params->adaptiveThreshConstant = 7;
	params->minMarkerPerimeterRate = 0.03;
	params->polygonalApproxAccuracyRate = 0.03;
	params->perspectiveRemovePixelPerCell = 4;
	params->perspectiveRemoveIgnoredMarginPerCell = 0.13;
	params->errorCorrectionRate = 0.6;
	params->cornerRefinementMethod = aruco::CORNER_REFINE_NONE;
	params->aprilTagQuadDecimate = 2;
	params->cornerRefinementMaxIterations = 30;
	params->cornerRefinementMinAccuracy = 0.1;
	params->cornerRefinementWinSize = 5;
	params->detectInvertedMarker = true;

	//cout << "apriltag number: " << aruco::CORNER_REFINE_APRILTAG << "\n";


	//uncomment to draw markers and save them to file, 12 per page
	/*		
	Ptr<aruco::GridBoard> markers = aruco::GridBoard::create(3, 4, 0.8, 0.2, dictionary, 0);
	Mat img;
	aruco::drawPlanarBoard(markers, Size(1240, 1754), img, 0, 2);
	imwrite("markers1.jpg", img);
	markers = aruco::GridBoard::create(3, 4, 0.8, 0.2, dictionary, 12);
	aruco::drawPlanarBoard(markers, Size(1240, 1754), img, 0, 2);
	imwrite("markers2.jpg", img);
	markers = aruco::GridBoard::create(3, 4, 0.8, 0.2, dictionary, 24);
	aruco::drawPlanarBoard(markers, Size(1240, 1754), img, 0, 2);
	imwrite("markers3.jpg", img);
	imshow("img", img);
	waitKey(0);
	*/

	FileStorage fs("test.yml", FileStorage::READ);		//open test.yml file to read parameters

	if (!fs["ip"].empty())			//if file exists, load all parameters from file into variables
	{
		fs["ip"] >> videoStreamAddress;			
		fs["markerSize"] >> markerSize;
		fs["cameraMatrix"] >> cameraMatrix;
		fs["distCoeffs"] >> distCoeffs;
		fs["usePredictive"] >> usePredictive;
		fs["driverSmoothFactor"] >> driverSmoothFactor;
		fs["smoothFactor"] >> smoothFactor;
		fs["numOfPrevValues"] >> numOfPrevValues;
		fs["smoothingType"] >> smoothingType;
		fs["redetectMarkers"] >> redetectMarkers;
		fs["useOpticalFlow"] >> useOpticalFlow;

		FileNode fn = fs["detectorParams"];
		if (!fn.empty())
		{
			fn["markerBorderBits"] >> params->markerBorderBits;
			fn["adaptiveThreshWinSizeMin"] >> params->adaptiveThreshWinSizeMin;
			fn["adaptiveThreshWinSizeMax"] >> params->adaptiveThreshWinSizeMax;
			fn["adaptiveThreshWinSizeStep"] >> params->adaptiveThreshWinSizeStep;
			fn["adaptiveThreshConstant"] >> params->adaptiveThreshConstant;
			fn["minMarkerPerimeterRate"] >> params->minMarkerPerimeterRate;
			fn["polygonalApproxAccuracyRate"] >> params->polygonalApproxAccuracyRate;
			fn["perspectiveRemovePixelPerCell"] >> params->perspectiveRemovePixelPerCell;
			fn["perspectiveRemoveIgnoredMarginPerCell"] >> params->perspectiveRemoveIgnoredMarginPerCell;
			fn["errorCorrectionRate"] >> params->errorCorrectionRate;
			fn["cornerRefinementMethod"] >> params->cornerRefinementMethod;
			fn["aprilTagQuadDecimate"] >> params->aprilTagQuadDecimate;
			fn["cornerRefinementMaxIterations"] >> params->cornerRefinementMaxIterations;
			fn["cornerRefinementMinAccuracy"] >> params->cornerRefinementMinAccuracy;
			fn["cornerRefinementWinSize"] >> params->cornerRefinementWinSize;
			fn["detectInvertedMarker"] >> params->detectInvertedMarker;
		}

		fn = fs["markers"];
		if (!fn.empty())		//load all saved markers
		{
			FileNodeIterator curMarker = fn.begin(), it_end = fn.end();
			for (; curMarker != it_end; ++curMarker)
			{
				vector<vector<Point3f>> boardCorners;
				vector<int> boardIds;
				FileNode item = *curMarker;
				item["markerIds"] >> boardIds;
				//data.push_back(tmp);
				FileNode fnCorners = item["markerCorners"];
				if (!fnCorners.empty())
				{
					FileNodeIterator curCorners = fnCorners.begin(), itCorners_end = fnCorners.end();
					for (; curCorners != itCorners_end; ++curCorners)
					{
						vector<Point3f> corners;
						FileNode item2 = *curCorners;
						item2 >> corners;
						boardCorners.push_back(corners);
					}
				}
				cout << boardIds.size() << "::" << boardCorners.size() << "\n";
				Ptr<aruco::Board> arBoard = aruco::Board::create(boardCorners, dictionary, boardIds);
				markerGroups.push_back(arBoard);
			}
		}
	}
	else
	{		//if parameter file doesnt exist ask for input of camera address and marker size
		cout << "Parameter file not found!\n Enter camera ip adress:";
		cin >> videoStreamAddress;
		cout << "Enter size of markers in meters:";
		cin >> markerSize;
	}

	cv::VideoCapture inputVideo;
	//const std::string videoStreamAddress = "http://192.168.1.102:8080/video";
	//open the video stream and make sure it's opened
	cout << " videostreamadress length " << videoStreamAddress.length() << "\n";
	if (videoStreamAddress.length() <= 1)		//if camera adress is a single character, try to open webcam
	{
		int i = stoi(videoStreamAddress);	//convert to int
		if (!inputVideo.open(i)) {			//open pc webcam on index
			std::cerr << "Error opening video stream or file!(webcam)" << std::endl;
			//inputVideo.open(0);

		}
	}
	else
	{			//if address is longer, we try to open it as an ip address
		if (!inputVideo.open(videoStreamAddress)) {
			std::cerr << "Error opening video stream or file!" << std::endl;
			//inputVideo.open(0);

		}
	}

	//try to reduce buffer size, not sure if it works
	cout << inputVideo.set(CAP_PROP_BUFFERSIZE, 0);

	//start image capturing and decoding in another thread
	thread imageThread(getImages, inputVideo);

	//print the camera matrix to check if parameters are loaded correctly
	cout << cameraMatrix << "\n";

	//close the file
	fs.release();

	//create the settings window
	namedWindow("settings",WINDOW_NORMAL);
	
	//add trackbars for each modifiable value
	createTrackbar("axis", "settings", &axisSize, 100);
	createTrackbar("calibration", "settings", &recalibrate, 1);
	createTrackbar("calib_x", "settings", &calibrate_x, 100);
	createTrackbar("calib_y", "settings", &calibrate_y, 100);
	createTrackbar("calib_z", "settings", &calibrate_z, 100);


	resizeWindow("settings", 500, 300);

	Quaternion<double> q;

	for (;;)	//start a function based on input number
	{
		cout << "1 to run, 2 to connect, 4 to add marker, 6 to calibrate camera" << "\n";
		int p;
		cin >> p;
		if (p == 1)
		{
			detect();
		}
		else if (p == 2)
		{
			connect();
		}
		else if (p == 3)
		{
			calibrateCamera();
		}
		else if (p == 4)
		{
			addMarker();
		}
		else if (p == 5)
		{
			tresholdTest();
		}
		else if (p == 6)
		{
			calibrateCameraCharuco();
		}
		else if (p == 7)
		{
			linearFitTest();
		}
	}
}

int linearFitTest()
{
	//a test function i used to test out linear fitting. It simply takes 10 values, fits a line, then
	//approximates what the last number should be.

	double x[10];
	vector<double>v;
	double a;
	double b;

	cout << "enter 10 y values \n";

	for (int i = 0; i < 10; i++)
	{
		x[i] = i;
		double val;
		cin >> val;
		v.push_back(val);
	}

	double* y = &v[0];

	llsq(10, x, y, a, b);

	for (int i = 0; i < 10; i++)
	{
		cout << a * i + b << ", ";
	}
	cout << "\n";

	for (;;)
	{
		double val;
		cin >> val;
		v.push_back(val);
		v.erase(v.begin());
		y = &v[0];

		llsq(10, x, y, a, b);
		cout << a * 9 + b << "\n ";
	}

	return 1;
}

int saveParams()
{
	//opening a file and writing current parameters to it
	FileStorage fs("test.yml", FileStorage::WRITE);

	fs << "ip" << videoStreamAddress;
	fs << "markerSize" << markerSize;
	fs << "usePredictive" << usePredictive;

	fs << "cameraMatrix" << cameraMatrix;
	fs << "distCoeffs" << distCoeffs;

	fs << "driverSmoothFactor" << driverSmoothFactor;
	fs << "smoothFactor" << smoothFactor;

	fs << "numOfPrevValues" << numOfPrevValues;
	fs << "smoothingType" << smoothingType;


	//writing all current markers to file
	fs << "markers";
	fs << "{";
	for (int i = 0; i < markerGroups.size(); i++)
	{
		fs << "marker_" + to_string(i);
		fs << "{";
		fs << "markerIds";
		fs << markerGroups[i]->ids;
		fs << "markerCorners";
		fs << "{";
		for (int j = 0; j < markerGroups[i]->objPoints.size(); j++)
		{
			fs << "markerCorners_" + to_string(j);
			fs << markerGroups[i]->objPoints[j];
		}
		fs << "}";
		fs << "}";
	}
	fs << "}";
	fs << "detectorParams";
	fs << "{";
	fs << "markerBorderBits" << params->markerBorderBits;
	fs << "adaptiveThreshWinSizeMin" <<  params->adaptiveThreshWinSizeMin;
	fs << "adaptiveThreshWinSizeMax" <<  params->adaptiveThreshWinSizeMax;
	fs << "adaptiveThreshWinSizeStep" <<  params->adaptiveThreshWinSizeStep;
	fs << "adaptiveThreshConstant" <<  params->adaptiveThreshConstant;
	fs << "minMarkerPerimeterRate" <<  params->minMarkerPerimeterRate;
	fs << "polygonalApproxAccuracyRate" <<  params->polygonalApproxAccuracyRate;
	fs << "perspectiveRemovePixelPerCell" <<  params->perspectiveRemovePixelPerCell;
	fs << "perspectiveRemoveIgnoredMarginPerCell" << params->perspectiveRemoveIgnoredMarginPerCell;
	fs << "errorCorrectionRate" <<  params->errorCorrectionRate;
	fs << "cornerRefinementMethod" <<  params->cornerRefinementMethod;
	fs << "aprilTagQuadDecimate" <<  params->aprilTagQuadDecimate;
	fs << "cornerRefinementMaxIterations" <<  params->cornerRefinementMaxIterations;
	fs << "cornerRefinementMinAccuracy" <<  params->cornerRefinementMinAccuracy;
	fs << "cornerRefinementWinSize" << params->cornerRefinementWinSize;
	fs << "detectInvertedMarker" << params->detectInvertedMarker;
	fs << "}";

	fs << "redetectMarkers" << redetectMarkers;
	fs << "useOpticalFlow" << useOpticalFlow;

	//closing file
	fs.release();

}

int tresholdTest()
{
	//test function to see how contour detecting works
	//it works the same as contour detecting in aruco module

	//load our current parameters
	int tresWindow = params->adaptiveThreshWinSizeMin;
	int tresConst = params->adaptiveThreshConstant + 50;
	double accuracyRate = params->polygonalApproxAccuracyRate;

	//add two new trackbars to control windowsize and treshold constant
	createTrackbar("Window", "settings", &tresWindow, 100);
	createTrackbar("Constant", "settings", &tresConst, 100);

	for (;;)
	{
		Mat image;
		Mat gray;
		//load image
		retImage.copyTo(image);

		cvtColor(image, gray, COLOR_BGR2GRAY);
		//window must be odd
		if (tresWindow % 2 == 0)
			tresWindow++;
		//do adaptive tresholding
		adaptiveThreshold(gray, gray, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, tresWindow,tresConst-50);

		//find all contours on tresholded image
		vector< vector< Point > > contours;
		findContours(gray, contours, RETR_LIST, CHAIN_APPROX_NONE);

		for (unsigned int i = 0; i < contours.size(); i++) {

			//approximate each contour to a simpler shape
			vector< Point > approxCurve;
			approxPolyDP(contours[i], approxCurve, double(contours[i].size()) * accuracyRate, true);
			if (approxCurve.size() != 4 || !isContourConvex(approxCurve)) continue;

			//if it is a rectangle, draw it
			polylines(image, approxCurve, true, Scalar(0, 255, 0),3);

		}

		//resize so both the tresholded and contour image are seen at the same time
		resize(gray, gray, Size(800, 600));
		resize(image, image, Size(800, 600));

		//show image
		imshow("out2",image);
		imshow("out1", gray);
		waitKey(1);
	}
}

int addMarker()
{
	//function to connect a group of markers to be tracked together


	//as markers will be recorded from a closer distance, we have to increase this parameter
	params->adaptiveThreshWinSizeMin = 23;
	params->adaptiveThreshWinSizeMax = 73;

	std::vector<int> ids;

	bool firstMarker = true;

	Mat image;

	vector<int> boardIds;

	//making a marker model of our markersize for later use
	vector<Point3f> testMarker1;
	vector<vector<Point3f>> boardCorners;
	testMarker1.push_back(Point3f(-markerSize / 2, markerSize / 2, 0));
	testMarker1.push_back(Point3f(markerSize / 2, markerSize / 2, 0));
	testMarker1.push_back(Point3f(markerSize / 2,-markerSize / 2, 0));
	testMarker1.push_back(Point3f(-markerSize / 2,-markerSize / 2, 0));

	//Ptr<aruco::GridBoard> arBoardGrid = aruco::GridBoard::create(1, 1, markerSize, 0.01, dictionary, 1);

	Vec3d boardRvec, boardTvec;

	bool markerVisible = false;
	int markerVisibleTimer = 0;

	while (true)
	{
		//create a board with our currently added markers
		Ptr<aruco::Board> arBoard = aruco::Board::create(boardCorners, dictionary, boardIds);

		//load image
		retImage.copyTo(image);

		clock_t start, end;
		//clock for timing of detection
		start = clock();

		//detect and draw all markers on image
		std::vector<int> ids;
		std::vector<std::vector<cv::Point2f> > corners;
		cv::aruco::detectMarkers(image, dictionary, corners, ids, params);
		aruco::drawDetectedMarkers(image, corners, ids);

		//estimate pose of our markers
		std::vector<cv::Vec3d> rvecs, tvecs;
		cv::aruco::estimatePoseSingleMarkers(corners, markerSize, cameraMatrix, distCoeffs, rvecs, tvecs);

		for (int i = 0; i < rvecs.size(); ++i) {
			//draw axis for each marker
			auto rvec = rvecs[i];	//rotation vector of our marker
			auto tvec = tvecs[i];	//translation vector of our marker

			//rotation/translation vectors are shown as offset of our camera from the marker

			aruco::drawAxis(image, cameraMatrix, distCoeffs, rvec, tvec, ((double)axisSize) / 200);
		}

		
		end = clock();
		//show the detection time
		cout << double(end - start) / double(CLOCKS_PER_SEC) << "\n";

		if (firstMarker)
		{
			//if no markers were added yet
			cv::imshow("out", image);
			char key = (char)cv::waitKey(1);
			if (key == -1)
			{
				//if no key was pressed
				continue;
			}
			//if a key was pressed, save the first marker
			for (int i = 0; i < rvecs.size(); ++i) {
				auto rvec = rvecs[i];
				auto tvec = tvecs[i];

				//add the marker id as first marker
				boardIds.push_back(ids[i]);

				//since it is the first marker, it has no offset so we can just add our model.
				boardCorners.push_back(testMarker1);
				firstMarker = false;
				break;
			}
			continue;
		}

		//Vec3d boardRvec, boardTvec;

		//if at least one marker was already added to our board, we can try to estimate the pose of our board
		//it returns how many markers in our board were detected
		if (aruco::estimatePoseBoard(corners, ids, arBoard, cameraMatrix, distCoeffs, boardRvec, boardTvec,markerVisible) > 0)
		{
			markerVisible = true;
			markerVisibleTimer = 0;

			//draw the axis of our board twice the size of others
			aruco::drawAxis(image, cameraMatrix, distCoeffs, boardRvec, boardTvec, (double)axisSize / 100);

			cv::imshow("out", image);
			char key = (char)cv::waitKey(1);
			if (key == 27)
			{
				//if ESC was pressed, we save the board to our group
				markerGroups.push_back(arBoard);
				//then we save params, saving our new marker as well
				saveParams();

				break;
			}
			if (key == -1)
			{
				//no button was pressed, go to next frame
				continue;
			}

			//if any button was pressed, we try to add visible markers to our board

			//convert the board rotation vector to rotation matrix
			Mat rmat;
			Rodrigues(boardRvec, rmat);

			//with rotation matrix and translation vector we create the translation matrix
			Mat mtranslation = Mat_<double>(4, 4);
			for (int x = 0; x < 3; x++)
			{
				for (int y = 0; y < 3; y++)
				{
					mtranslation.at<double>(x, y) = rmat.at<double>(x, y);
				}
			}
			for (int x = 0; x < 3; x++)
			{
				mtranslation.at<double>(x, 3) = boardTvec[x];
				mtranslation.at<double>(3, x) = 0;
			}
			mtranslation.at<double>(3, 3) = 1;

			//we check each marker
			for (int i = 0; i < rvecs.size(); ++i) {

				auto rvec = rvecs[i];
				auto tvec = tvecs[i];

				//aruco::drawAxis(image, cameraMatrix, distCoeffs, rvec, tvec, (double)axisSize / 200);

				//we check if the marker is already added to our board
				bool found = false;
				for (int j = 0; j < boardIds.size(); j++)
				{
					if (boardIds[j] == ids[i])
					{
						found = true;
					}
				}

				if (found)
				{
					//if marker is already added, we go to next one
					continue;
				}

				//othervise, we create our markers translation matrix
				Mat rmatin;
				Rodrigues(rvec, rmatin);

				Mat mtranslationin = Mat_<double>(4, 4);
				for (int x = 0; x < 3; x++)
				{
					for (int y = 0; y < 3; y++)
					{
						mtranslationin.at<double>(x, y) = rmatin.at<double>(x, y);
					}
				}
				for (int x = 0; x < 3; x++)
				{
					mtranslationin.at<double>(x, 3) = tvec[x];
					mtranslationin.at<double>(3, x) = 0;
				}
				mtranslationin.at<double>(3, 3) = 1;

				Mat rpos = Mat_<double>(4, 1);

				//we transform our model marker from our marker space to board space
				vector<Point3f> testMarker;
				for (int y = 0; y < testMarker1.size(); y++)
				{
					//transform corner of model marker from vector to mat for calculation
					rpos.at<double>(0, 0) = testMarker1[y].x;
					rpos.at<double>(1, 0) = testMarker1[y].y;
					rpos.at<double>(2, 0) = testMarker1[y].z;
					rpos.at<double>(3, 0) = 1;

					//multipy model marker corner with markers translation matrix to get its position in camera(global) space
					rpos = mtranslationin * rpos;
					//multiply marker corner in camera space with the inverse of the translation matrix of our board to put it into local space of our board
					rpos = mtranslation.inv() * rpos;

					//cout << ids[i] << " :: marker\n" << rpos << "\n";

					testMarker.push_back(Point3f(rpos.at<double>(0, 0), rpos.at<double>(1, 0), rpos.at<double>(2, 0)));
				}
				//we add current marker id to our board
				boardIds.push_back(ids[i]);
				//we add our marker corners to our board
				boardCorners.push_back(testMarker);

			}
		}
		else
		{
		//if board was not found, we just show image
			cv::imshow("out", image);
			char key = (char)cv::waitKey(1);
			if (key == 27)
			{
				//if ESC was pressed, save the board
				markerGroups.push_back(arBoard);
				params->adaptiveThreshWinSizeMin = 13;
				params->adaptiveThreshWinSizeMax = 33;
				saveParams();
				break;
			}
			if (key == -1)
			{
				//if no key was pressed, go to next frame
				continue;
			}
		}

		//if marker was not visible for 10 frames, we set markerVisible to false. This means
		//estimatePoseBoard will not take previous board position and rotation into account when calculating
		markerVisibleTimer++;
		if (markerVisibleTimer > 10)
		{
			markerVisible = false;
		}
	}

}

int calibrateCameraCharuco()
{
	//function to calibrate our camera

	bool success;

	Mat image;

	//generate and show our charuco board that will be used for calibration
	Ptr<aruco::CharucoBoard> board = aruco::CharucoBoard::create(9, 8, 0.04f, 0.02f, dictionary);
	Mat boardImage;
	board->draw(Size(1500, 1000), boardImage, 10, 1);
	imshow("calibration", boardImage);
	waitKey(1);

	vector<vector<Point2f>> allCharucoCorners;
	vector<vector<int>> allCharucoIds;

	//set our detectors marker border bits to 1 since thats what charuco uses
	params->markerBorderBits = 1;

	int i = 0;
	//get calibration data from 20 images
	while (i < 20)
	{
		//get and show image
		retImage.copyTo(image);
		cv::imshow("out", image);
		char key = (char)cv::waitKey(1);
		if (key != -1)
		{
			//if any button was pressed
			cvtColor(image, image, COLOR_BGR2GRAY);

			vector<int> markerIds;
			vector<vector<Point2f>> markerCorners;

			//detect our markers
			aruco::detectMarkers(image, dictionary, markerCorners, markerIds, params);

			if (markerIds.size() > 0)
			{
				//if markers were found, try to add calibration data
				vector<Point2f> charucoCorners;
				vector<int> charucoIds;
				//using data from aruco detection we refine the search of chessboard corners for higher accuracy
				aruco::interpolateCornersCharuco(markerCorners, markerIds, image, board, charucoCorners, charucoIds);
				if (charucoIds.size() > 5)
				{
					//if corners were found, we draw them
					aruco::drawDetectedCornersCharuco(image, charucoCorners, charucoIds);
					//we then add our corners to the array
					allCharucoCorners.push_back(charucoCorners);
					allCharucoIds.push_back(charucoIds);
					i++;
				}
			}
			
			cout << i << "\n";
			imshow("out", image);
			waitKey(0);
		}
	}

	//revert our border bits back to 2
	params->markerBorderBits = 2;

	Mat R, T;

	//calibrate camera using our data and save to our global params cameraMatrix and distCoeffs
	aruco::calibrateCameraCharuco(allCharucoCorners, allCharucoIds, board, Size(image.rows,image.cols), cameraMatrix, distCoeffs,R,T,0);
	
	//save all of the parameters
	saveParams();
	/*
	ofstream file;
	file.open("params.txt", ios::out || ios::trunc);

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			file << cameraMatrix.at<double>(i, j) << endl;
		}
	}
	for (int i = 0; i < 5; i++)
	{
		file << distCoeffs.at<double>(i) << endl;
	}

	cout << cameraMatrix << "\n" << distCoeffs << "\n";

	cout << "loooooooooooool\n";

	file.close();
	*/
}

int calibrateCamera()
{
	//old camera calibration using normal chessboard


	int CHECKERBOARD[2]{ 7,7 };

	vector<vector<Point3f>> objpoints;
	vector<vector<Point2f>> imgpoints;
	vector<Point3f> objp;

	for (int i{ 0 }; i < CHECKERBOARD[0]; i++)
	{
		for (int j{ 0 };j < CHECKERBOARD[1]; j++)
		{
			objp.push_back(Point3f(j, i, 0));
		}
	}

	cout << "aegf";

	vector<Point2f> corner_pts;
	bool success;

	Mat image;

	int i = 0;
	while (i < 10)
	{
		retImage.copyTo(retImage);
		cv::imshow("out", image);
		char key = (char)cv::waitKey(1);
		if (key != -1)
		{
			cvtColor(image, image, COLOR_BGR2GRAY);

			success = findChessboardCorners(image, Size(CHECKERBOARD[0], CHECKERBOARD[1]), corner_pts);

			if (success)
			{
				i++;
				TermCriteria criteria(TermCriteria::EPS | TermCriteria::MAX_ITER, 30, 0.001);

				cornerSubPix(image, corner_pts, Size(11, 11), Size(-1, -1), criteria);

				drawChessboardCorners(image, Size(CHECKERBOARD[0], CHECKERBOARD[1]), corner_pts, success);

				objpoints.push_back(objp);
				imgpoints.push_back(corner_pts);
			}
			cout << i << "\n";
			imshow("out", image);
			waitKey(0);
		}
	}

	Mat cameraMatrix, distCoeffs, R, T;

	calibrateCamera(objpoints, imgpoints, Size(image.rows, image.cols), cameraMatrix, distCoeffs, R, T);

	ofstream file;
	file.open("params.txt", ios::out||ios::trunc);

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			file << cameraMatrix.at<double>(i, j) << endl;
		}
	}
	for (int i = 0; i < 5; i++)
	{
		file << distCoeffs.at<double>(i) << endl;
	}

	cout << cameraMatrix << "\n" << distCoeffs << "\n";

	cout << "loooooooooooool\n";
	file.close();
}

int connect()
{
	//function to create pipes for SteamVR connection
	pipeNum = markerGroups.size();
	cout << pipeNum << "\n";

	if (hpipe.size() > 0)
	{
		//if pipes are already connected, return
		cout << "Connected to existing " << hpipe.size() << " pipes.";
		return 0;
	}

	for (int i = 0; i < pipeNum; i++)
	{
		//create a pipe with given name and index
		String pipeName = "\\\\.\\pipe\\TrackPipe" + to_string(i);
		HANDLE pipe;
		pipe = CreateNamedPipe(pipeName.c_str(),
			PIPE_ACCESS_DUPLEX,
			PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,   // FILE_FLAG_FIRST_PIPE_INSTANCE is not needed but forces CreateNamedPipe(..) to fail if the pipe already exists...
			1,
			1024 * 16,
			1024 * 16,
			NMPWAIT_USE_DEFAULT_WAIT,
			NULL);
		if (pipe != INVALID_HANDLE_VALUE)
		{
			//if pipe was successfully created wait for a connection
			cout << "waiting" << "\n";
			if (ConnectNamedPipe(pipe, NULL) != FALSE)   // wait for someone to connect to the pipe
			{
				cout << "connected pipe " << i+1 << "\n";

				//when pipe is connected, send number of pipes and driversmoothfactor to our connected driver
				String s = to_string(pipeNum) + " " + to_string(driverSmoothFactor);

				//write our data to pipe
				WriteFile(pipe,
					s.c_str(),
					(s.length() + 1),   // = length of string + terminating '\0' !!!
					&dwWritten,
					NULL);

				//Sleep(2000);
			}
		}
		//add our pipe to our global list of pipes
		hpipe.push_back(pipe);
	}
	//set that connection is established
	connectedPipe = true;
}

void getImages(VideoCapture inputVideo)
{
	//an infinite loop that will be reading images from our stream and writing them to our variable
	while (true)
	{
		inputVideo >> retImage;
		//set that new image is ready
		imgReady = true;
		//imshow("out", retImage);
	}
}

int detect()
{
	//main function that detects our marker groups and sends info to driver

	/*
	ifstream file;
	file.open("params.txt", ios::in);
	
	Mat camMat = Mat_<double>(3,3);
	Mat dist = Mat_<double>(1, 5);

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			file >> camMat.at<double>(i, j);
		}
	}
	for (int i = 0; i < 5; i++)
	{
		file >> dist.at<double>(i);
	}
	file.close();
	*/

	//Mat camMat = (Mat_<double>(3, 3) << 530.9645125, 0, 311.00548326, 0, 531.51104568, 238.6488721, 0, 0, 1);
	//Mat dist = (Mat_<double>(1, 5) << -0.11161805, 0.14274501, -0.00218158, 0.00362543, 0.29487485);

	std::vector<int> prevIds;
	std::vector<std::vector<cv::Point2f> > prevCorners;
	std::vector<int> ids;
	std::vector<std::vector<cv::Point2f> > corners;
	
	Mat  prevImg;

	Mat image, drawImg;
	//inputVideo >> image;

	cout << cameraMatrix << "\n";
	cout << distCoeffs << "\n";

	vector<Vec3d> boardRvec, boardTvec;
	vector<bool> boardFound;

	vector<Vec3d> prevLoc;
	vector<Quaternion<double>> prevRot;

	vector<vector<vector<double>>> prevLocValues;
	vector<double> prevLocValuesX;

	//set up our vectors that hold previous values of position and rotation.

	for (int k = 0; k < markerGroups.size(); k++)
	{
		vector<vector<double>> vv;
		for (int i = 0; i < 7; i++)
		{
			vector<double> v;
			for (int j = 0; j < numOfPrevValues; j++)
			{
				v.push_back(0.0);
			}
			vv.push_back(v);
		}	
		prevLocValues.push_back(vv);
	}

	//the X axis, it is simply numbers 0-10 (or the amount of previous values we have)
	for (int j = 0; j < numOfPrevValues; j++)
	{
		prevLocValuesX.push_back(j);
	}

	//init other variables for each marker group
	for (int i = 0; i < markerGroups.size(); i++)
	{
		boardRvec.push_back(Vec3d(0, 0, 0));
		boardTvec.push_back(Vec3d(0, 0, 0));
		boardFound.push_back(false);
		prevLoc.push_back(Vec3d(0, 0, 0));
		prevRot.push_back(Quaternion<double>());

	}

	for (;;)
	{
		//Mat image, drawImg;
		//inputVideo >> image;
		//resize(image, image, Size(2*640, 2*480));
		

		if (!imgReady)
		{
			//wait until image is ready
			continue;
		}
		
		imgReady = false;

		//imshow("out", retImage);
		//waitKey(1);

		//continue;

		//get our image
		retImage.copyTo(image);
		image.copyTo(drawImg);

		//if we set calibration on through the settings window, we call calibration
		if (recalibrate)
		{
			if (calibrate(cameraMatrix, distCoeffs, dictionary, image, params) != 0) 
			{
				//if calibration was unsuccessfull, we just show our image and go to next frame
				cv::imshow("out", drawImg);
				cv::waitKey(1);
				continue;
			}
			continue;
		}
		if(!calibrated)
		{
			//if calibration wasnt done yet and we are not calibrating, show image and go to next thread
			cv::imshow("out", drawImg);
			cv::waitKey(1);
			continue;
		}

		clock_t start, end;
		//for timing our detection
		start = clock();

		//detect markers
		std::vector<int> ids;
		std::vector<std::vector<cv::Point2f> > corners;
		std::vector<std::vector<cv::Point2f> > rejected;
		cv::aruco::detectMarkers(image, dictionary, corners, ids, params, rejected);

		end = clock();
		//time of marker detection
		cout << double(end - start)/double(CLOCKS_PER_SEC) << "\n";
		
		//draw rejected candidates
		for (std::vector< vector<Point2f> >::iterator it = rejected.begin(); it != rejected.end(); ++it) {
			vector<Point2f> sqPoints = *it;
			//cout << sqPoints.size() << endl;
			//Point pt2(it[1].x, it[1].y);
			line(drawImg, sqPoints[0], sqPoints[1], CV_RGB(255, 0, 0));
			line(drawImg, sqPoints[2], sqPoints[1], CV_RGB(255, 0, 0));
			line(drawImg, sqPoints[2], sqPoints[3], CV_RGB(255, 0, 0));
			line(drawImg, sqPoints[0], sqPoints[3], CV_RGB(255, 0, 0));
		}

		//if optical flow is used, we try to get markers that were detected previous frame but not in current frame
		//using optical flow algorithms. It is mostly unnecessary and too slow.
		if (useOpticalFlow)
		{

			for (int i = 0; i < prevIds.size(); i++)
			{
				bool found = false;
				for (int j = 0; j < ids.size(); j++)
				{
					if (prevIds[i] == ids[j])
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					vector<uchar> status;
					vector<float> err;
					vector<Point2f> tempCorners;
					calcOpticalFlowPyrLK(prevImg, image, prevCorners[i], tempCorners, status, err, winSize, 3, termcrit, 0, 0.001);

					double arc = arcLength(tempCorners, true);
					double tol = arc * 0.1;

					for (int x = 0; x < rejected.size(); x++)
					{
						bool foundMarker = true;
						for (int y = 0; y < tempCorners.size(); y++)
						{
							bool foundCorner = false;
							for (int z = 0; z < rejected[x].size(); z++)
							{
								double lenx = abs(rejected[x][z].x - tempCorners[y].x);
								double leny = abs(rejected[x][z].y - tempCorners[y].y);

								double len = sqrt(lenx * lenx + leny * leny);

								if (len < tol)
								{
									foundCorner = true;
									break;
								}
							}
							if (foundCorner == false)
							{
								foundMarker = false;
								break;
							}
						}
						if (foundMarker == true)
						{
							ids.push_back(prevIds[i]);
							corners.push_back(tempCorners);
							break;
						}
					}
				}
			}
			prevIds = ids;
			prevCorners = corners;
			image.copyTo(prevImg);
		}


		//cv::imshow("out", drawImg);
		//cv::waitKey(1);
		
		
		//std::vector<cv::Vec3d> rvecs, tvecs;
		//cv::aruco::estimatePoseSingleMarkers(corners, markerSize, cameraMatrix, distCoeffs, rvecs, tvecs);

		for (int i = 0; i < markerGroups.size(); ++i) {
			
			//Vec3d  rvec, tvec;

			//using detected markers and our board info we try to redetect markers that havent been detected
			if (redetectMarkers)
			{
				aruco::refineDetectedMarkers(image, markerGroups[i], corners, ids, rejected, cameraMatrix, distCoeffs, 10, -1, false);
			}

			//estimate the pose of current board
			if (aruco::estimatePoseBoard(corners, ids, markerGroups[i], cameraMatrix, distCoeffs, boardRvec[i], boardTvec[i],boardFound[i] && usePredictive) <= 0)
			{
				//board was not found, set variable and go to next board
				boardFound[i] = false;
				continue;
			}

			boardFound[i] = true;

			//cv::aruco::drawAxis(drawImg, cameraMatrix, distCoeffs, boardRvec[i], boardTvec[i], (double)axisSize/100);

			Mat rpos = Mat_<double>(4, 1);

			//transform boards position based on our calibration data

			for (int x = 0; x < 3; x++)
			{
				rpos.at<double>(x, 0) = boardTvec[i][x];
			}
			rpos.at<double>(3, 0) = 1;
			rpos = wtranslation * rpos;

			//convert rodriguez rotation to quaternion
			Quaternion<double> q = rodr2quat(boardRvec[i][0], boardRvec[i][1], boardRvec[i][2]);

			//mirror our rotation
			q.z = -q.z;
			q.w = -q.w;

			//some unused transformations
			Quaternion<double> qrot = Quaternion<double>(0, 0, 1, 0);
			Quaternion<double> qrot2 = Quaternion<double>(0.707, 0, 0, -0.707);
			
			//q = wrotation * q;
			//q = qrot * q;

			double a = rpos.at<double>(1, 0);
			double b = -rpos.at<double>(2, 0);
			double c = rpos.at<double>(0, 0);		

			//smoothing type 1 means smoothing using least squares linear fitting
			if (smoothingType == 1)
			{
				//turn current values into single array
				double posValues[7] = { a,b,c,q.x,q.y,q.z,q.w };

				for (int j = 0; j < 7; j++)
				{
					//push new values into previous values list end and remove the one on beggining
					prevLocValues[i][j].push_back(posValues[j]);
					prevLocValues[i][j].erase(prevLocValues[i][j].begin());

					double avar;
					double bvar;

					double* y = &prevLocValues[i][j][0];
					double* x = &prevLocValuesX[0];

					//use previous values for line fitting
					llsq(numOfPrevValues, x, y, avar, bvar);

					//change current value to fit our line
					posValues[j] = (numOfPrevValues-1) * avar + bvar;

					prevLocValues[i][j][numOfPrevValues-1] = posValues[j];

				}

				//save fitted values back to our variables
				a = posValues[0];
				b = posValues[1];
				c = posValues[2];
				q.x = posValues[3];
				q.y = posValues[4];
				q.z = posValues[5];
				q.w = posValues[6];
			}
			//use smoothing by using x * previous + (1-x) * current
			else if (smoothingType == 0)
			{
				double factor;
				factor = smoothFactor;

				a = (1 - factor) * prevLoc[i][0] + (factor)*a;
				b = (1 - factor) * prevLoc[i][1] + (factor)*b;
				c = (1 - factor) * prevLoc[i][2] + (factor)*c;

				q = q.UnitQuaternion();

				//to ensure we rotate quaternion into correct direction
				double dot = q.x * prevRot[i].x + q.y * prevRot[i].y + q.z * prevRot[i].z + q.w * prevRot[i].w;

				if (dot < 0)
				{
					q.x = (factor)*q.x - (1 - factor) * prevRot[i].x;
					q.y = (factor)*q.y - (1 - factor) * prevRot[i].y;
					q.z = (factor)*q.z - (1 - factor) * prevRot[i].z;
					q.w = (factor)*q.w - (1 - factor) * prevRot[i].w;
				}
				else
				{
					q.x = (factor)*q.x + (1 - factor) * prevRot[i].x;
					q.y = (factor)*q.y + (1 - factor) * prevRot[i].y;
					q.z = (factor)*q.z + (1 - factor) * prevRot[i].z;
					q.w = (factor)*q.w + (1 - factor) * prevRot[i].w;
				}

				q = q.UnitQuaternion();
			}

			//save values for next frame
			prevRot[i] = q;
			prevLoc[i] = Vec3d(a, b, c);
			

			//save position to string
			string s;
			s = to_string(a) +
				" " + to_string(b) +
				" " + to_string(c) +
				" " + to_string(q.w) +
				" " + to_string(q.x) +
				" " + to_string(q.y) +
				" " + to_string(q.z) + "\n";

			//cout << index << "\n";

			//send the string to our driver
			if (connectedPipe)
			{

				WriteFile(hpipe[i],
					s.c_str(),
					(s.length() + 1),   // = length of string + terminating '\0' !!!
					&dwWritten,
					NULL);
			}
		}

		//if markers were detected, draw them
		if (ids.size() > 0)
			cv::aruco::drawDetectedMarkers(drawImg, corners, ids);

		cv::imshow("out", drawImg);
		char key = (char)cv::waitKey(1);
		if (key == 27)
			break;
	}
	return 0;
}

int calibrate(Mat cameraMatrix, Mat disctCoeffs, Ptr<aruco::Dictionary> dictionary, Mat frame, Ptr<aruco::DetectorParameters> params)
{

	//cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
	//cv::adaptiveThreshold(frame, frame, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY,51,2);

	std::vector<int> ids;
	std::vector<std::vector<cv::Point2f> > corners;
	std::vector<std::vector<cv::Point2f> > rejected;

	/*
	std::vector<std::vector<cv::Point2f> > allCorners;
	vector<Point2f> curpointv;

	Point3d bigMarker[4];
	
	bigMarker[0] = Point3d(0.25, 0, 0.5);
	bigMarker[1] = Point3d(0.25, 0, 1);
	bigMarker[2] = Point3d(-0.25, 0, 1);
	bigMarker[3] = Point3d(-0.25, 0, 0.5);

	int curCorner = 0;

	while(curCorner < 4)
	{

		if (!imgReady)
		{
			continue;
		}

		imgReady = false;

		frame = retImage;

		cv::aruco::detectMarkers(frame, dictionary, corners, ids, params);
		aruco::drawDetectedMarkers(frame, corners, ids);

		if (recalibrate == 0)
		{
			if (ids.size() > 0) {
				cout << "why\n";
				for (int i = 0; i < ids.size(); i++)
				{
					if (ids[i] == 1)
					{
						cout << "what\n";
						Point2f curpoint;
						curpoint.x = (corners[i][0].x + corners[i][1].x + corners[i][2].x + corners[i][3].x) / 4;
						curpoint.y = (corners[i][0].y + corners[i][1].y + corners[i][2].y + corners[i][3].y) / 4;
						curpointv.push_back(curpoint);
						recalibrate = 1;
						curCorner++;
					}
				}
			}
		}

		string s;
		s = to_string(bigMarker[curCorner].x) +
			" " + to_string(bigMarker[curCorner].y) +
			" " + to_string(bigMarker[curCorner].z) +
			" " + to_string(0) +
			" " + to_string(0) +
			" " + to_string(0) +
			" " + to_string(0) + "\n";

		//cout << index << "\n";
		if (connectedPipe)
		{

			WriteFile(hpipe[0],
				s.c_str(),
				(s.length() + 1),   // = length of string + terminating '\0' !!!
				&dwWritten,
				NULL);
		}

		imshow("out", frame);
		waitKey(1);

	}
	Point2f curpoint;
	Point2f center;
	center.x = (curpointv[0].x + curpointv[2].x) / 2;
	center.y = (curpointv[0].y + curpointv[2].y) / 2;

	curpoint.x = center.x - (curpointv[1].x - center.x);
	curpoint.y = center.y - (curpointv[1].y - center.y);

	//curpointv.push_back(curpoint);

	recalibrate = 0;
	allCorners.push_back(curpointv);

	cv::aruco::drawDetectedMarkers(frame, allCorners);

	*/

	//function to sync our camera space and steamVr space

	//send the position we are calibrating to driver. Default this is 1 meter above ground in center of play space, but can be changed.
	string s;
	s = to_string(((double)calibrate_z - 50) / 10) +
		" " + to_string(((double)calibrate_x-50)/10) +
		" " + to_string(((double)calibrate_y-50) / 10) +
		" " + to_string(0) +
		" " + to_string(0) +
		" " + to_string(0) +
		" " + to_string(0) + "\n";

	//cout << index << "\n";
	if (connectedPipe)
	{
		WriteFile(hpipe[0],
			s.c_str(),
			(s.length() + 1),   // = length of string + terminating '\0' !!!
			&dwWritten,
			NULL);
	}


	//detect and draw markers
	cv::aruco::detectMarkers(frame, dictionary, corners, ids, params);

	aruco::drawDetectedMarkers(frame, corners);

	Mat rmat;
	cv::Vec3d rvec, tvec;
	
	//cv::aruco::estimatePoseSingleMarkers(corners, markerSize, cameraMatrix, disctCoeffs, rvecs, tvecs);

	//redetect markers
	aruco::refineDetectedMarkers(frame, markerGroups[0], corners, ids, rejected, cameraMatrix, distCoeffs);

	//first board is used for calibration
	if (aruco::estimatePoseBoard(corners, ids, markerGroups[0], cameraMatrix, distCoeffs, rvec, tvec) > 0)
	{	

		//tvec[0] += 0.5;
		//tvec[2] += 2;
		//tvec[1] += 1;

		//if board was found, we generate a transformation matrix for it
		Rodrigues(rvec, rmat);
		cv::aruco::drawAxis(frame, cameraMatrix, disctCoeffs, rvec, tvec, (double)axisSize/100);
		wtranslation = Mat_<double>(4, 4);
		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				wtranslation.at<double>(x, y) = rmat.at<double>(x, y);
			}
		}
		for (int x = 0; x < 3; x++)
		{
			wtranslation.at<double>(x, 3) = tvec[x];
			wtranslation.at<double>(3, x) = 0;
		}
		wtranslation.at<double>(3, 3) = 1;
		wtranslation = wtranslation.inv();
		//we add the values we sent to our driver to our matrix
		wtranslation.at<double>(0, 3) += ((double)calibrate_y-50) / 10;
		wtranslation.at<double>(1, 3) += ((double)calibrate_z-50) / 10;
		wtranslation.at<double>(2, 3) -= ((double)calibrate_x-50) / 10;
		wrotation = rodr2quat(rvec[0], rvec[1], rvec[2]);
		wrotation = wrotation.conjugate();

		//cout << wtranslation << "\n";
		//cout << wrotation.w << ":" << wrotation.x << ":" << wrotation.y << ":" << wrotation.z << ":" << "\n";
		//cout << rvec << "\n";

		//show image and set calibrated to true
		cv::imshow("out", frame);
		waitKey(1);
		calibrated = true;
		return 0;
	}
	imshow("out", frame);
	waitKey(1);
	return 3;
}

//code bellow here is from other sources

void llsq(int n, double x[], double y[], double& a, double& b)

//****************************************************************************80
//
//  Purpose:
//
//    LLSQ solves a linear least squares problem matching a line to data.
//
//  Discussion:
//
//    A formula for a line of the form Y = A * X + B is sought, which
//    will minimize the root-mean-square error to N data points ( X[I], Y[I] );
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    17 July 2011
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int N, the number of data values.
//
//    Input, double X[N], Y[N], the coordinates of the data points.
//
//    Output, double &A, &B, the slope and Y-intercept of the least-squares
//    approximant to the data.
//
{
	double bot;
	int i;
	double top;
	double xbar;
	double ybar;
	//
	//  Special case.
	//
	if (n == 1)
	{
		a = 0.0;
		b = y[0];
		return;
	}
	//
	//  Average X and Y.
	//
	xbar = 0.0;
	ybar = 0.0;
	for (i = 0; i < n; i++)
	{
		xbar = xbar + x[i];
		ybar = ybar + y[i];
	}
	xbar = xbar / (double)n;
	ybar = ybar / (double)n;
	//
	//  Compute Beta.
	//
	top = 0.0;
	bot = 0.0;
	for (i = 0; i < n; i++)
	{
		top = top + (x[i] - xbar) * (y[i] - ybar);
		bot = bot + (x[i] - xbar) * (x[i] - xbar);
	}
	a = top / bot;

	b = ybar - a * xbar;

	return;
}

// Calculates rotation matrix given euler angles.
Mat eulerAnglesToRotationMatrix(double a, double b, double c)
{
	a = a * 0.01745;
	b = b * 0.01745;
	c = c * 0.01745;
	// Calculate rotation about x axis
	Mat R_x = (Mat_<double>(3, 3) <<
		1, 0, 0,
		0, cos(a), -sin(a),
		0, sin(a), cos(a)
		);

	// Calculate rotation about y axis
	Mat R_y = (Mat_<double>(3, 3) <<
		cos(b), 0, sin(b),
		0, 1, 0,
		-sin(b), 0, cos(b)
		);

	// Calculate rotation about z axis
	Mat R_z = (Mat_<double>(3, 3) <<
		cos(c), -sin(c), 0,
		sin(c), cos(c), 0,
		0, 0, 1);


	// Combined rotation matrix
	Mat R = R_z * R_y * R_x;

	return R;

}

Quaternion<double> rodr2quat(double x, double y, double z)
{
	
	double theta;
	theta = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	x = x / theta;
	y = y / theta;
	z = z / theta;

	double qx = x * sin(theta / 2);
	double qy = y * sin(theta / 2);
	double qz = z * sin(theta / 2);
	double qw = cos(theta / 2);

	Quaternion<double> q(qw,qx,qy,qz);

	return q;
}


Quaternion<double> mRot2Quat(const Mat& m) {
	double r11 = m.at<double>(0, 0);
	double r12 = m.at<double>(0, 1);
	double r13 = m.at<double>(0, 2);
	double r21 = m.at<double>(1, 0);
	double r22 = m.at<double>(1, 1);
	double r23 = m.at<double>(1, 2);
	double r31 = m.at<double>(2, 0);
	double r32 = m.at<double>(2, 1);
	double r33 = m.at<double>(2, 2);
	double q0 = (r11 + r22 + r33 + 1.0f) / 4.0f;
	double q1 = (r11 - r22 - r33 + 1.0f) / 4.0f;
	double q2 = (-r11 + r22 - r33 + 1.0f) / 4.0f;
	double q3 = (-r11 - r22 + r33 + 1.0f) / 4.0f;
	if (q0 < 0.0f) {
		q0 = 0.0f;
	}
	if (q1 < 0.0f) {
		q1 = 0.0f;
	}
	if (q2 < 0.0f) {
		q2 = 0.0f;
	}
	if (q3 < 0.0f) {
		q3 = 0.0f;
	}
	q0 = sqrt(q0);
	q1 = sqrt(q1);
	q2 = sqrt(q2);
	q3 = sqrt(q3);
	if (q0 >= q1 && q0 >= q2 && q0 >= q3) {
		q0 *= +1.0f;
		q1 *= SIGN(r32 - r23);
		q2 *= SIGN(r13 - r31);
		q3 *= SIGN(r21 - r12);
	}
	else if (q1 >= q0 && q1 >= q2 && q1 >= q3) {
		q0 *= SIGN(r32 - r23);
		q1 *= +1.0f;
		q2 *= SIGN(r21 + r12);
		q3 *= SIGN(r13 + r31);
	}
	else if (q2 >= q0 && q2 >= q1 && q2 >= q3) {
		q0 *= SIGN(r13 - r31);
		q1 *= SIGN(r21 + r12);
		q2 *= +1.0f;
		q3 *= SIGN(r32 + r23);
	}
	else if (q3 >= q0 && q3 >= q1 && q3 >= q2) {
		q0 *= SIGN(r21 - r12);
		q1 *= SIGN(r31 + r13);
		q2 *= SIGN(r32 + r23);
		q3 *= +1.0f;
	}
	else {
		printf("coding error\n");
	}
	double r = NORM(q0, q1, q2, q3);
	q0 /= r;
	q1 /= r;
	q2 /= r;
	q3 /= r;

	Quaternion<double> q;
	q.x = q1;
	q.y = q2;
	q.z = q3;
	q.w = q0;

	return q;
}
Quaternion<double> slerp(Quaternion<double> v0, Quaternion<double> v1, double t) {
	// Only unit quaternions are valid rotations.
	// Normalize to avoid undefined behavior.
	v0 = v0.UnitQuaternion();
	v0 = v1.UnitQuaternion();

	// Compute the cosine of the angle between the two vectors.
	double dot = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z + v0.w * v1.w;

	// If the dot product is negative, slerp won't take
	// the shorter path. Note that v1 and -v1 are equivalent when
	// the negation is applied to all four components. Fix by 
	// reversing one quaternion.
	if (dot < 0.0f) {
		v1 = Quaternion<double>(-v1.w,-v1.x,-v1.y,-v1.z);
		dot = -dot;
	}

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD) {
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.

		Quaternion<double> result = v0 + (v1 - v0).scale(t);
		return result.UnitQuaternion();
	}

	// Since dot is in range [0, DOT_THRESHOLD], acos is safe
	double theta_0 = acos(dot);        // theta_0 = angle between input vectors
	double theta = theta_0 * t;          // theta = angle between v0 and result
	double sin_theta = sin(theta);     // compute this value only once
	double sin_theta_0 = sin(theta_0); // compute this value only once

	double s0 = cos(theta) - dot * sin_theta / sin_theta_0;  // == sin(theta_0 - theta) / sin(theta_0)
	double s1 = sin_theta / sin_theta_0;
	
	return (v0.scale(s0) + (v1.scale(s1)));
}
// END OF PROGRAM